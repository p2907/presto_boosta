<?php

namespace App\Models;

use App\Models\Article;
use Spatie\Image\Image;
use App\Models\ArticleImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ArticleImage extends Model
{
  //  protected $fillable = [
  //       'file',
  //   ];

  protected $casts = [
    'labels' => 'array',
  ];
  
    use HasFactory;
    
    public function announcement()
    {
      return $this->belongsTo(Article::class);
    }

    static public function GetUrlByFilePath($filePath, $w = null, $h = null){
      if(!$w && !$h){
        return Storage::url($filePath);
      }

      $path = dirname($filePath);
      $filename = basename($filePath);
      $file = "{$path}/crop{$w}x{$h}_{$filename}";
      return Storage::url($file);
    }

    public function getUrl($w = null, $h = null){
      return ArticleImage::GetUrlByFilePath($this->file, $w, $h);
    }
}
