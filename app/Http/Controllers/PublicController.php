<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class PublicController extends Controller
{

    public function homepage()
    {
        $articles = Article::orderBy('created_at', 'DESC')
            ->paginate(5)
            ->where('is_accepted', true);
        $categories = Category::all();
        return view("welcome", compact('articles', 'categories'));
    }

    public function search(Request $request){

        $q = $request->input('q') . $request->input('price');
        $articles = Article::search($q)->where('is_accepted', true)->get();
        return view('article_search', compact('q', 'articles'));
    }

    public function index(){
        $articles = Article::where('is_accepted',true)
        ->orderBy('created_at','DESC')
        ->take(5)
        ->get();
        return view("welcome", compact('articles'));
    }

    public function articlesByCategory($name , $category_id)
    {
        $category = Category::find($category_id);
        $articles = $category
        ->articles()
        ->where('is_accepted', true)
        ->orderBy('created_at','DESC')
        ->paginate(5);
        return view('articles',compact('articles', 'categories'));
    }



    public function showCategory(Category $category)
    {
        $articles=$category->articles()->get();
        return view('article', compact('category','articles'));
    }

    public function locale($locale){
        session()->put('locale', $locale);
        return redirect()->back();
    }

}
