<?php

namespace App\Http\Controllers;

use index;
use App\Models\User;
use App\Models\Article;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
// use App\Http\Controllers\RevisorController;

class RevisorController extends Controller
{   
    
  

    public function __construct(){
    
        $this->middleware('auth.revisor');
    }
    
    
    
    public function index(Request $request){
        
        $user = User::all();
        $article = Article::where('is_accepted', null) 
        ->orderBy('created_at','desc')
        ->first();
        // Mail::to($request)->send(new ContactMail($contact));
        return view('home' , compact('article', 'user'));
    }

    private function setAccepted($article_id , $value){
        
        $article = Article::find($article_id);
        $article->is_accepted = $value; 
        $article->save(); 
        
        return redirect(route('revisor.home'));

    }

    public function accept($article_id){
        return $this->setAccepted($article_id,true);
    }

    public function reject($article_id){
        return $this->setAccepted($article_id,false);
    }







}
