<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\ArticleImage;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use App\Http\Requests\ArticleRequest;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;

class ArticleController extends Controller
{


    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=Article::orderBy('created_at', 'DESC')
            ->where('is_accepted', true)
            ->paginate(4);

        return view("article.index", compact('articles'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $uniqueSecret = $request->old(
        'uniqueSecret' ,
        base_convert(sha1(uniqid(mt_rand())) ,
        16,36 ));

        return view("article.create", compact('uniqueSecret'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $a = Article::create([

            'title'=>$request->title,
            'price'=>$request->price,
            'description'=>$request->description,
            /* 'img'=>$request->file('img')->store('public/img'),  */
            
        ]);

        /* $uniqueSecret = $request->input('uniqueSecret', []); */
        $uniqueSecret = $request->uniqueSecret;
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removeImages.{$uniqueSecret}", []);
        $images = array_diff($images, $removedImages);

        // USER STORY 5, CONTROLLARE QUESTA PARTE. Min. Video 41 //

        foreach ($images as $image) {
            
        $i = new ArticleImage();
        
        $fileName = basename($image);
        // SE NON FUNZIONA LEVARE /
        $newFileName = "public/articles/{$a->id}/{$fileName}"; 
        Storage::move($image, $newFileName);

        
        $i->file = $newFileName;
        $i->article_id = $a->id;
         
        $i->save();

        GoogleVisionSafeSearchImage::withChain([
            new GoogleVisionLabelImage ($i->id),
            new GoogleVisionRemoveFaces ($i->id),
            new ResizeImage($i->file, 400, 250)
        ])->dispatch($i->id);


        }

        /* File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}")); */

        $a->categories()->attach($request->categories);
        return redirect(route('article.index'))->with('message', 'Il tuo articolo è stato inviato al revisor con successo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $category = Category::all();
        return view('article.edit', compact('article', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $article->update([
            'title'=>$request->title,
            'price'=>$request->price,
            'description'=>$request->description,
            'img'=>$request->img,
    ]);

    $article->category()->sync($request->categories);
    return redirect(route("article.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->categories()->detach();
        $article->user()->dissociate();
        $article->delete();
    }

    public function imagesUpload(Request $request){
        // dd($request->input());
        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");
        
        dispatch(new ResizeImage(
            $fileName,
            80, 80
        ));

        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(
            // session()->get("images.{$uniqueSecret}")
            [
                'id'=>$fileName,
            ]
        );
    }

    public function removeImage(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $fileName);
        Storage::delete($fileName);
        return response()->json('ok');
    }

    public function getImages(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];
        
        foreach ($images as $image) {
            $data[] = [
                'id' => $image,
                'src' => ArticleImage::getUrlByFilePath($image, 80, 80)
            ];
        }
        return response()->json($data);
    }
    

}
