<x-layout>
    
    <x-navbar></x-navbar>
    

        @if ($article)
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-4 col-md-4 card-custom">
                    @foreach ($article->adImages as $image)
                    <img class="rounded py-3 w-75" src="{{$image->getUrl(400, 250)}}" alt="">
                    @break
                    @endforeach 
                    <p class="lead mt-4">Titolo: {{$article->title}}</p>
                    <p class="lead">Data: {{$article->created_at->format('d/m/Y')}}</p>
                    <p class="lead">Prezzo: {{$article->price}} euro</p>
                    <a href="#" class="btn btn-custom text-white">Dettaglio</a>
                </div>
                <div class="col-12 col-md-6">
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                <h3>Annuncio # {{$article->id}}</h3>
                                <h3 class="mb-5">Utente: {{Auth::user()->name}}</h3>
                                <p class="lead">Titolo: {{$article->title}}</p>
                                <p class="lead">Data: {{$article->created_at->format('d/m/Y')}}</p>
                                <p class="lead">Descrizione: {{$article->description}}</p>
                                <p class="lead">Prezzo: {{$article->price}} euro</p>
                            </div>
                            <div class="col-6">
                                @foreach ($article->adImages as $image)
                                <img src="{{$image->getUrl(400, 250)}}" class="rounded" alt="">
                           
                                    Adult: {{ $image->adult }} 
                                    Medical: {{ $image->medical }} 
                                    Spoof: {{ $image->spoof }} 
                                    Violence: {{ $image->violence }} 
                                    Racy: {{ $image->racy }} 
                                    
                                    <b>Labels</b>
                                    <ul>
                                        @if ($image->labels)
                                        @foreach ($image->labels as $label)
                                        <li> {{ $label }} </li>
                                        @endforeach
                                        @endif
                                    </ul>   
                                    
                                @endforeach 
                            </div>
                        </div>
                    </div>
                        
                      
                    
                    
                    {{-- <img class="rounded" src="https://picsum.photos/500/300" alt=""> --}}
                       
                    <div class="container">
                        <div class="row justify-content-center mt-5">
                            <div class="col-md-6 text-center">
                                <form action="{{route('revisor.accept', $article->id ) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-success">ACCETTA</button>
                                </form>
                            </div>
                            <div class="col-md-6 text-right">
                                <form action="{{route('revisor.reject', $article->id ) }}" method="POST">
                                    @csrf
                                <button type="submit" class="btn btn-danger">RIFIUTA</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        @else
        <div class="container">
            <div class="row text-center">
                <div class="col-10">
                    <h3 class="display-4 mt-5">Non ci sono articoli da revisionare</h3>
                    <img class="img-fluid w-75 mt-5" src="{{Storage::url('img/pandiavolo.jpg')}}" alt="">
                </div>
            </div>
        </div>
        
        @endif
        




        <x-footer></x-footer>
        
</x-layout>