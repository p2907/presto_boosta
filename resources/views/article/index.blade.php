<x-layout>

    <x-navbar></x-navbar>

    <x-slot name='title'> articoli</x-slot>

    {{-- <div class="container">
        <div class="row">
                <h1>vediamo se funziona</h1>
                @if (session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                  </div>

                @endif
                @foreach ($articles as $article)
                <div class="col-12 col-md-4">
                 <div class="card">
                    <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">Nome: {{$article->name}}</h5>
                      <h5 class="card-title">Categoria: coming soon</h5>
                      <h5 class="card-title">Prezzo: {{$article->price}}</h5>
                      <h5 class="card-title">Data: {{$article->created_at}}</h5>
                      <a href="{{route("article.show", compact('article'))}}" class="btn btn-primary">Dettaglio</a>
                    </div>

                  </div>
                  @endforeach
                </div>
        </div>

    </div> --}}
    @if (session('message'))
    <div class="alert text-center text-white mt-5 bg-alert-custom">
        {{session('message')}}
      </div>
    @endif


    <div class="container">
      <div class="row justify-content-between">
        @foreach ($articles as $article)
        <div class="col-12 col-md-4 card-custom mt-5">
          {{-- <img class="img-fluid rounded py-3" src="https://picsum.photos/250/300" alt="Card image cap"> --}}
          @foreach ($article->adImages as $image)
          <img class="rounded py-3 w-75" src="{{$image->getUrl(400, 250)}}" alt="">
          @break
          @endforeach 
          <h5>Nome: {{$article->title}}</h5>

          @if(count($article->categories))
            @foreach ($article->categories as $category)
              <p>Categoria: {{$category->name_category}}</p>
            @endforeach
          @endif

          <p>Prezzo: {{$article->price}} euro</p>
          <p>Data: {{$article->created_at->format('m/d/Y')}}</p>
          <a href="{{route("article.show", compact('article'))}}" class="btn btn-custom text-white">Dettaglio</a>
        </div>
        @endforeach
      </div>
      <div class="container mt-5">
            {{$articles->links()}}
      </div>
    </div>



    <x-footer></x-footer>
</x-layout>