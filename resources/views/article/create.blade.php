<x-layout>

    <x-navbar></x-navbar>

    @if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <h2 class="display-4 mt-5 text-center">Inserisci il tuo Annuncio</h2>
            <div class="col-12 col-md-6 mt-5">
             <img class="w-75" src="{{Storage::url('img/login-img.png')}}" alt="">
            </div>
            <div class="col-12 col-md-6 mt-5">
             <form action="{{route('article.store')}}" method="POST"  enctype="multipart/form-data">
                 @csrf
                 {{-- <meta name="csrf-token" content="{{ csrf_token() }}">
                 <meta name="uniqueSecret" content="{{ $uniqueSecret }}"> --}}

                 <input type="hidden"
                 name="uniqueSecret"
                 value="{{$uniqueSecret}}">


                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label lead">Titolo del tuo annuncio</label>
                  <input type="text" value="{{old('title')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="title">
                </div>
                <div class="container my-5">
                    <div class="row justify-content-center">
                        <h4 class="text-center mb-4">Categorie</h4>

                        {{-- prova database --}}

                    <select name="categories[]" multiple>
                      @foreach ($categories as $category)
                      <option value="{{$category->id}}">
                      {{$category->name_category}}
                      </option>

                      @endforeach
                    </select>

                    </div>
                </div>
                 <div class="mb-3 container">
                   <div class="row">
                       <label for="exampleInputPassword1" class="form-label lead">Corpo Annuncio
                       </label>
                       <div class="col-12">
                        <textarea class="w-100" name="description" id="" cols="30" rows="10"></textarea>
                       </div>
                   </div>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPrice" class="form-label lead">Prezzo</label>
                    <input type="number" name="price" value="{{old('price')}}" class="form-control" id="exampleInputPrice">
                </div>
                {{-- <div class="mb-3">
                  <label class="form-label lead">Carica un immagine</label>
                  <input type="file" name="img">
              </div> --}}
              <div>
                <label for="images">Immagini</label>
                <div class="dropzone" id="drophere"></div>
    </div>
                 <button class="mt-3 btn btn-custom text-white" type="submit" class="btn btn-custom">Inserisci</a>
               </form>
            </div>
        </div>
    </div>
    <main class="py-4">
    </main>




    <x-footer></x-footer>
    </x-layout>