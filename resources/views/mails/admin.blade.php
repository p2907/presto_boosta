<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style type="text/css">
        * {font-family: 'Lato', sans-serif;}
         a {color:#EA7575;}
    </style>

</head>
<body>

    <h1>Ciao amministratore</h1>
    <p>Hai una nuova richiesta revisor:</p>
    <p>L'utente {{$forAdmin['user']}} ha richiesto di diventare un revisor.</p>
    <p>Motivazione: {{$forAdmin['description']}}</p>
    <br>
    <h3>Questo è il suo contatto email: <a href="">{{$forAdmin['email']}}</a></h3>
    
</body>
</html>