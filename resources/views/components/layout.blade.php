<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Presto</title>
    <!-- nostro Style -->
    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <!-- font google -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Lato&display=swap" rel="stylesheet">
    <link rel="icon" href="{{Storage::url('img/occhietti.png')}}">
    {{-- aggiungere il csrf --}}
    <meta name="csrf-token" content="{{csrf_token()}}">


</head>
<body>

{{$slot}}


  <!-- nostro Style -->
  <script src="{{asset("js/app.js")}}"></script>
</body>
</html>