
{{-- Footer --}}
<footer class="text-center text-white mt-5 bg-footer" >
  <div class="row">
    <div class="col-6">
      <!-- Grid container -->
      <div class="container pt-4">
        <!-- Section: Social media -->
        <section class="mb-4">
          <h5 class="text-black">I nostri social:</h5>
          <!-- Facebook -->
          <a
            class="btn btn-link btn-floating btn-lg text-dark m-1"
            href="#!"
            role="button"
            data-mdb-ripple-color="dark"
            ><img src="https://img.icons8.com/fluency/48/000000/facebook.png"/></a>
    
          <!-- Twitter -->
          <a
            class="btn btn-link btn-floating btn-lg text-dark m-1"
            href="#!"
            role="button"
            data-mdb-ripple-color="dark"
            ><img src="https://img.icons8.com/color/48/000000/twitter--v1.png"/></a>
    
          <!-- Google -->
          <a
            class="btn btn-link btn-floating btn-lg text-dark m-1"
            href="#!"
            role="button"
            data-mdb-ripple-color="dark"
            ><img src="https://img.icons8.com/color/48/000000/google-logo.png"/></a>
    
          <!-- Instagram -->
          <a
            class="btn btn-link btn-floating btn-lg text-dark m-1"
            href="#!"
            role="button"
            data-mdb-ripple-color="dark"
            ><img src="https://img.icons8.com/color/48/000000/instagram-new--v2.png"/></a>
    
        </section>
        <!-- Section: Social media -->
        <h5 class="text-black">Contattaci a questo indirizzo email: <br><a class="text-decoration-none email-footer" href="">amministrazione@presto.com</a></h5>
      </div>
    </div>
    <div class="col-6">
      <img class="mt-4 mb-3" src="{{Storage::url('img/logo_navbar.png')}}" alt="">
        <p class="text-black">
          Acquisti rapidi e sicuri per tutti i nostri utenti. <br> E come diciamo sempre: "la vita corre, <br> ma i nostri corrieri corrono di più"
        </p>
    </div>
  </div>
    
    <!-- Grid container -->
  <div class="line-footer"></div>
    <!-- Copyright -->
    <div class="text-center text-dark p-3">
        Powered by Boosta
    </div>
    <!-- Copyright -->
  </footer>