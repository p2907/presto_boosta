<x-layout>

<x-navbar></x-navbar>

@if (session('message'))
  <div class="alert alert-success">

    {{session('message')}}

  </div>

@endif

@if (session('access.denied.revisor.only'))
  <div class="alert alert-error">

    Accesso non consentito - solo per revisori

  </div>

@endif

        <header class="container-fluid m-0 p-0 img-header">
            <div class="row h-100">
                <div class="container-fluid m-0 p-0 bg-custom-search w-75">
                  <form action="{{route('search')}}" method="GET">  
                  <div class="row justify-content-center align-items-center">
                        <h2 class="display-5 text-center mt-3 mb-3">{{ __('ui.search_articles') }}</h2>
                        
                        
                        <div class="col-6 col-md-4 margin-search">
                            <h4>{{ __('ui.what_find') }}</h4>
                            <input class="form-control" type="text" name="q" placeholder="moto, auto, elettronica...">
                        </div>
                        <div class="col-6 col-md-4">
                            <h4>{{ __('ui.price') }}</h4>
                            <input class="form-control" type="number" name="price">
                        </div>
                        <div class="col-12 col-md-3">
                          <button class="btn btn-custom margin-top-search" type="submit"><img src="https://img.icons8.com/ios-glyphs/30/000000/search--v2.png"/></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </header>


        {{-- <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <h2 class="display-4">{{ __('ui.welcome') }}</h2>
            </div>
          </div>
        </div> --}}

        
{{-- sezione category --}}
 <div class="container-fluid my-5 pt-5">
     <h2  class="text-center mt-5">{{ __('ui.category') }}</h2>
        <div class="container mt-3 wrapper-categories d-flex justify-content-center ">
          <div class="row w-100 text-center justify-content-center align-items-center bg-white row-category ">
              @foreach ($categories as $category)
            <div class="col-6 col-md-2 ms-1">
              <a class="text-decoration-none" href="{{route('article.category', compact('category'))}}">
                {{-- <div class="col-6 col-md-6 col-lg-4 col-xl-2 px-0"> --}}
                <div class="b-radius-card card-category d-flex flex-column justify-content-evenly align-items-center">
                  <img src="{{$category->image_category}}" alt="">
                  <h6>{{$category->name_category}}</h6>
                </div>
              </a>
            </div>
              @endforeach

          </div>
        </div>
  </div>




{{-- ULTIMI ANNUNCI --}}

<section class="margin-custom">
<h2 class="text-center">{{ __('ui.last_announce') }}</h2>
<div class="container">
    <div class="row justify-content-around">
      @foreach ($articles as $article)
      <div class="col-12 col-md-4 card-custom mt-5">
        {{-- <img class="img-fluid rounded py-3" src="https://picsum.photos/250/300" alt="Card image cap"> --}}
        @foreach ($article->adImages as $image)
        <img class="rounded py-3 w-75" src="{{$image->getUrl(400, 250)}}" alt="">
        @break
        @endforeach 
        <h5>Nome: {{$article->title}}</h5>
        @if(count($article->categories))
          @foreach ($article->categories as $category)
            <p class="lead">Categoria: {{$category->name_category}}</p>
          @endforeach
        @endif
        <p class="lead">Prezzo: {{$article->price}} euro</p>
        <p class="lead">Data: {{$article->created_at->format('d/m/Y')}}</p>
        <a href="{{route("article.show", compact('article'))}}" class="btn btn-custom text-white">Dettaglio</a>
      </div>
      @endforeach
    </div>
  </div>

</section>

<x-footer></x-footer>


</x-layout>