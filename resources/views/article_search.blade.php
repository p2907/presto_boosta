<x-layout>
<x-navbar></x-navbar>

<h1 class="text-center display-4 mt-5">Risultati ricerca {{$q}}</h1>
<div class="container-fluid my-5 pt-5>
    <div class="row justify-content-center">
      @foreach ($articles as $article)
      <div class="col-12 col-md-4 card-custom mt-5 mx-4">
        {{-- <img class="img-fluid rounded py-3" src="https://picsum.photos/250/300" alt="Card image cap"> --}}
        @foreach ($article->adImages as $image)
        <img class="rounded py-3 w-75" src="{{$image->getUrl(400, 250)}}" alt="">
        @break
        @endforeach 
        <h5>Nome: {{$article->title}}</h5>

         {{-- @if(count($article->categories))
          @foreach ($article->categories as $category)
            <p>Categoria: {{$category->name_category}}</p>
          @endforeach
        @endif --}}

        <p>Prezzo: {{$article->price}}</p>
        <p>Data: {{$article->created_at->format('d/m/Y')}}</p>
        <a href="{{route("article.show", compact('article'))}}" class="btn btn-custom text-white">Dettaglio</a>
      </div>
      @endforeach
    </div>
    

    <x-footer></x-footer>
</x-layout>