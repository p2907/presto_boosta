<x-layout>

<x-navbar></x-navbar>


<h3 class="text-center display-4 mt-5">Diventa ora un Revisor</h3>



<div class="container">
    <div class="row">
        <div class="col-10 col-md-8">
            <form method="POST" action="{{route('revisor.submit')}}" >
                @csrf
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Nome Utente</label>
                  <input type="text" class="form-control" id="exampleInputName" name="user">
                </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                </div>
                <div class="mb-3">
                    <label for="exampleInputMotivation1" class="form-label">Motivazione</label>
                    <textarea class="w-100" id="text" name="description" placeholder="Inserisci il motivo per cui vuoi diventare Revisor..." cols="30" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-custom text-white">Submit</button>
              </form>
        </div>
    </div>
</div>


<x-footer></x-footer>
</x-layout>