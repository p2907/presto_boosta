<?php

return [
    'category' => 'explore our categories',
    'search_articles' => "Find your next purchase",
    'what_find' => "What are you looking for?",
    'price' => "Price",
    'last_announce' => "Latest Announcements",
    'sign_in' => "Login",
    'register' => "Sign In",
    'articles_nav' => "Articles",
    'logout' => "Logout",
    'announce' => "Post Ad",
    'job_with_us' => "Work with us",
    'revisor' => "Auditor's Home",
    'announce_bar' => "Post Ad",
    'language'=>"Choose your language ",
    'welcome' => "Welcome",
    'access' => "Log in",
    'account' => "Already a member?"
];