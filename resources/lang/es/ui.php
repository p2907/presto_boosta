<?php

return [
    'category' => 'explora nuestras categorías',
    'search_articles' => "Encuentra tu próxima compra",
    'what_find' => "¿Qué estás buscando?",
    'price' => "Precio",
    'last_announce' => "Últimos Anuncios",
    'sign_in' => "Acceso",
    'register' => "Iniciar Sesión",
    'articles_nav' => "Artículos",
    'logout' => "Cerrar Sesión",
    'announce' => "Publicar anuncio",
    'job_with_us' => "Trabaja con nosotros",
    'revisor' => "Home del auditor",
    'announce_bar' => "Publicar anuncio",
    'language'=>"Elige tu idioma",
    'welcome' => "Bienvenido",
    'access' => "Iniciar sesiòn",
    'account' => "¿Ya eres miembro?"
];